﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class BoxToBox : Agent
{
    public float spd = 1;
    public GameObject cosasBloques;
    private Transform target;
    bool canJump = true;

    public override void OnEpisodeBegin()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GreenCube");
        target = go.transform;
        canJump = true;
        transform.localPosition = new Vector3(0, 1, 0);
        print("REINICIANDO");
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target.localPosition);

    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];
        int salto = (int)Mathf.Floor(actions.ContinuousActions[2]);
        transform.localPosition += new Vector3(moveX, 0, moveZ) * Time.deltaTime * spd;
        print(target.position);
        if (actions.ContinuousActions[2]>=0.5)
        {
            if (canJump)
            {
                StartCoroutine("Jump");
            }
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> acCont = actionsOut.ContinuousActions;
        acCont[0] = Input.GetAxisRaw("Horizontal");
        acCont[1] = Input.GetAxisRaw("Vertical");
        if (Input.GetKey("space"))
        {
            acCont[2] = 1;
        }
        else
        {
            acCont[2] = 0;
        }
    }

    IEnumerator Jump()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        canJump = false;
        this.transform.GetChild(0).gameObject.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
       
        if (collision.transform.tag == "wall")
        {
            AddReward(-1f);
            EndEpisode();
        }else if(collision.transform.tag == "GreenCube")
        {
            AddReward(0.1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "GreenCube")
        {
            AddReward(1f);
            EndEpisode();
            cosasBloques.gameObject.GetComponent<MapGenerator>().HaTocadoBloque();
        }
        else if (other.transform.tag == "wall"){
            AddReward(-1f);
            EndEpisode();
        }
        canJump = true;
    }
}
