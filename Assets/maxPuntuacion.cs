﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maxPuntuacion : MonoBehaviour
{
    private int puntuacionActual;
    public GameObject agente;

    // Update is called once per frame
    void Update()
    {
        if(puntuacionActual< agente.GetComponent<BoxToBox>().puntuacion)
        {
            puntuacionActual = agente.GetComponent<BoxToBox>().puntuacion;
        }
        this.GetComponent<TextMesh>().text = "" + puntuacionActual;
    }
}
