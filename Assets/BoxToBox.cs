﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class BoxToBox : Agent
{
    public float spd = 1;
    public GameObject cosasBloques;
    private Transform target;
    bool canJump = true;
    public GameObject go;
    public int puntuacion = 0;
    public override void OnEpisodeBegin()
    {
       // go = GameObject.FindGameObjectWithTag("GreenCube");
        //print("SOY EL BOXTOBOX NUMERO NO SE K Y SIGO A ESTE CUBO " + go.transform.name);
        target = go.transform;
        puntuacion = 0;
        canJump = true;
        transform.localPosition = new Vector3(0, 1, 0);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target.localPosition);
    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];
        int salto = (int)Mathf.Floor(actions.ContinuousActions[2]);
        if (salto==1)
        {
            if (canJump)
            {
                StartCoroutine(Jump());
            }
        }
        transform.localPosition += new Vector3(moveX, 0, moveZ) * Time.deltaTime * spd;
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> acCont = actionsOut.ContinuousActions;
        acCont[0] = Input.GetAxisRaw("Horizontal");
        acCont[1] = Input.GetAxisRaw("Vertical");
        acCont[2] = Input.GetKey(KeyCode.Space) ? 1.0f : 0.0f;
    }

    IEnumerator Jump()
    {
        canJump = false;
        this.transform.GetChild(0).gameObject.SetActive(true);
        this.GetComponent<Rigidbody>().AddForce(new Vector3(0, 2, 0)*1.2f,ForceMode.Impulse);
        yield return new WaitForSeconds(5);
        this.transform.GetChild(0).gameObject.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
       
        if (collision.transform.tag == "wall")
        {
            AddReward(-1f);
            EndEpisode();
        }else if(collision.transform.tag == "GreenCube")
        {
            AddReward(10f);
        }
        canJump = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "GreenCube")
        {
            AddReward(100f);
            //EndEpisode();
            puntuacion++;
            cosasBloques.gameObject.GetComponent<MapGenerator>().HaTocadoBloque();
            canJump = true;
        }else
        {
           AddReward(-1f);
           //EndEpisode();
        }
    }
}
